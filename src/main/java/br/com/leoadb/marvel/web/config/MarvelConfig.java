package br.com.leoadb.marvel.web.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan({ "br.com.leoadb.marvel.service", "br.com.leoadb.marvel.service.impl" })
public class MarvelConfig {

}

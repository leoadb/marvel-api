package br.com.leoadb.marvel.web.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.leoadb.marvel.model.MarvelCreator;
import br.com.leoadb.marvel.model.MarvelSerie;
import br.com.leoadb.marvel.service.MarvelCreatorService;
import br.com.leoadb.marvel.service.MarvelSerieService;

@RestController
@RequestMapping(value = "/marvel/api", produces = "application/json; charset=UTF-8")
public class MarvelController {

	@Autowired
	private MarvelSerieService marvelSerieService;

	@Autowired
	private MarvelCreatorService marvelCreatorService;

	@Value("${pageSize}")
	private int pageSize;

	@RequestMapping(value = "/series", method = RequestMethod.GET)
	public List<MarvelSerie> findSeries(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		int offset = (page - 1) * (pageSize - 1);

		List<MarvelSerie> marvelSeries = marvelSerieService.findSeries(offset, pageSize);

		return marvelSeries;
	}

	@RequestMapping(value = "/series/{id}", method = RequestMethod.GET)
	public MarvelSerie findSerie(@PathVariable("id") int id) {
		return marvelSerieService.findSerie(id);
	}

	@RequestMapping(value = "/creators", method = RequestMethod.GET)
	public List<MarvelCreator> findCreators(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		int offset = (page - 1) * (pageSize - 1);

		List<MarvelCreator> marvelCreators = marvelCreatorService.findCreators(offset, pageSize);

		return marvelCreators;
	}

	@RequestMapping(value = "/creators/{id}", method = RequestMethod.GET)
	public MarvelCreator findCreator(@PathVariable("id") int id) {
		return marvelCreatorService.findCreator(id);
	}
}

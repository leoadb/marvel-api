package br.com.leoadb.marvel.web.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.leoadb.marvel.service.MarvelServiceException;

@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

	private static Logger log = Logger.getLogger(GlobalControllerExceptionHandler.class.getName());

	@ExceptionHandler(Throwable.class)
	@ResponseBody
	public ResponseEntity<Object> handleControllerException(HttpServletRequest req, Throwable ex) {
		log.error(ex);
		Map<String, String> responseBody = new HashMap<>();
		if (ex instanceof MarvelServiceException) {
			responseBody.put("message", ex.getMessage());
			ex.printStackTrace();
			return new ResponseEntity<Object>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		responseBody.put("path", req.getRequestURI());
		responseBody.put("message", "This is not a valid URL request.");
		return new ResponseEntity<Object>(responseBody, HttpStatus.NOT_FOUND);
	}
}

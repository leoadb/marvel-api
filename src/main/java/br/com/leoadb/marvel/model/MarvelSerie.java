package br.com.leoadb.marvel.model;

import java.util.ArrayList;
import java.util.List;

public class MarvelSerie {
	private int id;
	private String title;
	private String description;
	private int startYear;
	private int endYear;
	private List<MarvelSerieCreator> creators;

	public MarvelSerie() {
		creators = new ArrayList<>();
	}

	public MarvelSerie(int id, String title, String description, int startYear, int endYear) {
		this();
		this.id = id;
		this.title = title;
		this.description = description;
		this.startYear = startYear;
		this.endYear = endYear;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStartYear() {
		return startYear;
	}

	public void setStartYear(int startYear) {
		this.startYear = startYear;
	}

	public int getEndYear() {
		return endYear;
	}

	public void setEndYear(int endYear) {
		this.endYear = endYear;
	}

	public List<MarvelSerieCreator> getCreators() {
		return creators;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarvelSerie other = (MarvelSerie) obj;
		if (id != other.id)
			return false;
		return true;
	}

}

package br.com.leoadb.marvel.model;

public class MarvelCreatorEvent {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MarvelCreatorEvent(String name) {
		this.name = name;
	}
}

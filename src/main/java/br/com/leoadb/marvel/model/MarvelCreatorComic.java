package br.com.leoadb.marvel.model;

public class MarvelCreatorComic {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MarvelCreatorComic(String name) {
		this.name = name;
	}
}

package br.com.leoadb.marvel.model;

public class MarvelCreatorStory {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MarvelCreatorStory(String name) {
		this.name = name;
	}
}

package br.com.leoadb.marvel.model;

import java.util.ArrayList;
import java.util.List;

public class MarvelCreator {
	private int id;
	private String name;
	private List<MarvelCreatorSerie> series;
	private List<MarvelCreatorEvent> events;
	private List<MarvelCreatorComic> comics;
	private List<MarvelCreatorStory> stories;

	public MarvelCreator() {
		series = new ArrayList<>();
		events = new ArrayList<>();
		comics = new ArrayList<>();
		stories = new ArrayList<>();
	}

	public MarvelCreator(int id, String name) {
		this();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MarvelCreatorSerie> getSeries() {
		return series;
	}

	public List<MarvelCreatorEvent> getEvents() {
		return events;
	}

	public List<MarvelCreatorComic> getComics() {
		return comics;
	}

	public List<MarvelCreatorStory> getStories() {
		return stories;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarvelCreator other = (MarvelCreator) obj;
		if (id != other.id)
			return false;
		return true;
	}

}

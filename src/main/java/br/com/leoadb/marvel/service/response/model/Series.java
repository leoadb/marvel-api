package br.com.leoadb.marvel.service.response.model;

public class Series {
	private int id;
	private String title;
	private String description;
	private int startYear;
	private int endYear;
	private CreatorList creators;

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public int getStartYear() {
		return startYear;
	}

	public int getEndYear() {
		return endYear;
	}

	public CreatorList getCreators() {
		return creators;
	}

}

package br.com.leoadb.marvel.service.response.model;

public class CreatorDataContainer {
	private int total;
	private Creator[] results;

	public int getTotal() {
		return total;
	}

	public Creator[] getResults() {
		return results;
	}
}

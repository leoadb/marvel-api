package br.com.leoadb.marvel.service.response.model;

public class CreatorSummary {
	private String name;
	private String role;

	public String getName() {
		return name;
	}

	public String getRole() {
		return role;
	}

}

package br.com.leoadb.marvel.service.response.model;

public class Creator {
	private int id;
	private String fullName;
	private SeriesList series;
	private StoryList stories;
	private ComicList comics;
	private EventList events;

	public int getId() {
		return id;
	}

	public String getFullName() {
		return fullName;
	}

	public SeriesList getSeries() {
		return series;
	}

	public StoryList getStories() {
		return stories;
	}

	public ComicList getComics() {
		return comics;
	}

	public EventList getEvents() {
		return events;
	}

}

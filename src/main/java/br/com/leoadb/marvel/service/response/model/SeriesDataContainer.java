package br.com.leoadb.marvel.service.response.model;

public class SeriesDataContainer {
	private int total;
	private Series[] results;

	public int getTotal() {
		return total;
	}

	public Series[] getResults() {
		return results;
	}

}

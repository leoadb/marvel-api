package br.com.leoadb.marvel.service;

import java.util.List;

import br.com.leoadb.marvel.model.MarvelCreator;

public interface MarvelCreatorService {
	public List<MarvelCreator> findCreators(int offset, int limit);

	public MarvelCreator findCreator(int id);
}

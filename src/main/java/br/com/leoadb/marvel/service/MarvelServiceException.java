package br.com.leoadb.marvel.service;

@SuppressWarnings("serial")
public class MarvelServiceException extends RuntimeException {

	public MarvelServiceException() {
	}

	public MarvelServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public MarvelServiceException(String message) {
		super(message);
	}

	public MarvelServiceException(Throwable cause) {
		super(cause);
	}
}

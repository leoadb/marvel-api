package br.com.leoadb.marvel.service.util;

import java.security.MessageDigest;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MarvelSettings {

	private static Logger log = Logger.getLogger(MarvelSettings.class.getName());

	@Value("${marvel.api.key}")
	private String apiKey;

	@Value("${marvel.api.key.private}")
	private String apiPrivateKey;

	@Value("${marvel.api.base.url}")
	private String baseUrl;

	private String createHash(long timestamp) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			String input = String.format("%d%s%s", System.currentTimeMillis(), apiPrivateKey, apiKey);
			return HexUtils.toHexString(md.digest(input.getBytes("UTF-8")));
		} catch (Exception ex) {
			log.error("An error occured while creating Marvel hash.", ex);
			return "";
		}
	}

	public String mountSeriesUrl(int offset, int limit) {
		long timestamp = System.currentTimeMillis();
		String hash = createHash(timestamp);
		return String.format("%s/v1/public/series?ts=%d&apikey=%s&hash=%s&offset=%d&limit=%d&orderBy=title", baseUrl,
				timestamp, apiKey, hash, offset, limit);
	}

	public String mountCreatorUrl(int offset, int limit) {
		long timestamp = System.currentTimeMillis();
		String hash = createHash(timestamp);
		return String.format("%s/v1/public/creators?ts=%d&apikey=%s&hash=%s&offset=%d&limit=%d&orderBy=firstName",
				baseUrl, timestamp, apiKey, hash, offset, limit);
	}

	public String mountSeriesUrl(int id) {
		long timestamp = System.currentTimeMillis();
		String hash = createHash(timestamp);
		return String.format("%s/v1/public/series/%d?ts=%d&apikey=%s&hash=%s", baseUrl, id, timestamp, apiKey, hash);
	}
	
	public String mountCreatorUrl(int id) {
		long timestamp = System.currentTimeMillis();
		String hash = createHash(timestamp);
		return String.format("%s/v1/public/creators/%d?ts=%d&apikey=%s&hash=%s", baseUrl, id, timestamp, apiKey, hash);
	}
}

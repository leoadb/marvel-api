package br.com.leoadb.marvel.service.response.model;

public class CreatorDataWrapper {
	private int code;
	private CreatorDataContainer data;
	private String etag;

	public int getCode() {
		return code;
	}

	public CreatorDataContainer getData() {
		return data;
	}

	public String getEtag() {
		return etag;
	}
}

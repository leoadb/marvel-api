package br.com.leoadb.marvel.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;

import br.com.leoadb.marvel.model.MarvelCreator;
import br.com.leoadb.marvel.service.MarvelCreatorService;
import br.com.leoadb.marvel.service.MarvelServiceException;
import br.com.leoadb.marvel.service.response.model.CreatorDataWrapper;
import br.com.leoadb.marvel.service.util.MarvelDataExtractor;
import br.com.leoadb.marvel.service.util.MarvelSettings;

@Service
public class MarvelCreatorServiceImpl extends MarvelBaseService implements MarvelCreatorService {

	@Autowired
	protected MarvelSettings marvelSettings;

	@Autowired
	protected MarvelDataExtractor marvelDataExtractor;

	private Map<Integer, MarvelCreator> marvelCreatorMap = new ConcurrentHashMap<>();
	protected Map<String, String> etagMap = new ConcurrentHashMap<>();
	protected Map<String, List<Integer>> listResultMap = new ConcurrentHashMap<>();

	private List<MarvelCreator> parseCreatorJSON(String etagMapKey, String json) {
		Gson gson = new Gson();
		CreatorDataWrapper creatorDataWrapper = gson.fromJson(json, CreatorDataWrapper.class);

		if (creatorDataWrapper == null) {
			return null;
		}

		etagMap.put(etagMapKey, creatorDataWrapper.getEtag());

		List<MarvelCreator> marvelCreators = marvelDataExtractor.getCreators(creatorDataWrapper);

		List<Integer> ids = new ArrayList<>(marvelCreators.size());

		for (MarvelCreator marvelCreator : marvelCreators) {
			ids.add(marvelCreator.getId());
			marvelCreatorMap.put(marvelCreator.getId(), marvelCreator);
		}

		listResultMap.put(etagMapKey, ids);

		return marvelCreators;
	}

	@Override
	public List<MarvelCreator> findCreators(int offset, int limit) {
		ClientResponse response = null;
		try {
			String url = marvelSettings.mountCreatorUrl(offset, limit);
			String etagMapKey = String.format("findCreators_%d_%d", offset, limit);

			Map<String, String> headers = new HashMap<>();

			if (etagMap.containsKey(etagMapKey)) {
				headers.put("If-None-Match", etagMap.get(etagMapKey));
			}

			response = sendRequest(url, headers);

			if (response.getStatus() == 304) {
				List<Integer> ids = listResultMap.get(etagMapKey);
				List<MarvelCreator> resultList = new ArrayList<>(ids.size());
				for (Integer id : ids) {
					resultList.add(marvelCreatorMap.get(id));
				}
				return resultList;
			}

			String json = response.getEntity(String.class);

			return parseCreatorJSON(etagMapKey, json);

		} catch (Exception ex) {
			throw new MarvelServiceException("An error occured while finding creators.", ex);
		} finally {
			try {
				response.close();
			} catch (Exception ex) {
				throw new MarvelServiceException("An error occured while finding creators.", ex);
			}
		}
	}

	@Override
	public MarvelCreator findCreator(int id) {
		ClientResponse response = null;
		try {
			String url = marvelSettings.mountCreatorUrl(id);
			String etagMapKey = String.format("findCreator_%d", id);

			Map<String, String> headers = new HashMap<>();

			if (etagMap.containsKey(etagMapKey)) {
				headers.put("If-None-Match", etagMap.get(etagMapKey));
			}

			response = sendRequest(url, headers);

			if (response.getStatus() == 304) {
				return marvelCreatorMap.get(id);
			}

			String json = response.getEntity(String.class);

			List<MarvelCreator> marvelCreators = parseCreatorJSON(etagMapKey, json);

			if (marvelCreators.isEmpty()) {
				return null;
			}

			return marvelCreators.get(0);
		} catch (Exception ex) {
			throw new MarvelServiceException("An error occured while finding a creator by id.", ex);
		} finally {
			try {
				response.close();
			} catch (Exception ex) {
				throw new MarvelServiceException("An error occured while finding a creator by id.", ex);
			}
		}
	}

}

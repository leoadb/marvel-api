package br.com.leoadb.marvel.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;

import br.com.leoadb.marvel.model.MarvelSerie;
import br.com.leoadb.marvel.service.MarvelSerieService;
import br.com.leoadb.marvel.service.MarvelServiceException;
import br.com.leoadb.marvel.service.response.model.SeriesDataWrapper;
import br.com.leoadb.marvel.service.util.MarvelDataExtractor;
import br.com.leoadb.marvel.service.util.MarvelSettings;

@Service
class MarvelSerieServiceImpl extends MarvelBaseService implements MarvelSerieService {

	@Autowired
	protected MarvelSettings marvelSettings;

	@Autowired
	protected MarvelDataExtractor marvelDataExtractor;

	private Map<Integer, MarvelSerie> marvelSerieMap = new ConcurrentHashMap<>();
	protected Map<String, String> etagMap = new ConcurrentHashMap<>();
	protected Map<String, List<Integer>> listResultMap = new ConcurrentHashMap<>();

	private List<MarvelSerie> parseSerieJSON(String etagMapKey, String json) {
		Gson gson = new Gson();
		SeriesDataWrapper seriesDataWrapper = gson.fromJson(json, SeriesDataWrapper.class);

		if (seriesDataWrapper == null) {
			return null;
		}

		etagMap.put(etagMapKey, seriesDataWrapper.getEtag());

		List<MarvelSerie> marvelSeries = marvelDataExtractor.getSeries(seriesDataWrapper);

		List<Integer> ids = new ArrayList<>(marvelSeries.size());

		for (MarvelSerie marvelSerie : marvelSeries) {
			ids.add(marvelSerie.getId());
			marvelSerieMap.put(marvelSerie.getId(), marvelSerie);
		}

		listResultMap.put(etagMapKey, ids);

		return marvelSeries;
	}

	@Override
	public List<MarvelSerie> findSeries(int offset, int limit) {
		ClientResponse response = null;
		try {
			String url = marvelSettings.mountSeriesUrl(offset, limit);
			String etagMapKey = String.format("findSeries_%d_%d", offset, limit);

			Map<String, String> headers = new HashMap<>();

			if (etagMap.containsKey(etagMapKey)) {
				headers.put("If-None-Match", etagMap.get(etagMapKey));
			}

			response = sendRequest(url, headers);

			if (response.getStatus() == 304) {
				List<Integer> ids = listResultMap.get(etagMapKey);
				List<MarvelSerie> resultList = new ArrayList<>(ids.size());
				for (Integer id : ids) {
					resultList.add(marvelSerieMap.get(id));
				}
				return resultList;
			}

			String json = response.getEntity(String.class);

			return parseSerieJSON(etagMapKey, json);

		} catch (Exception ex) {
			throw new MarvelServiceException("An error occured while finding series.", ex);
		} finally {
			try {
				response.close();
			} catch (Exception ex) {
				throw new MarvelServiceException("An error occured while finding series.", ex);
			}
		}
	}

	@Override
	public MarvelSerie findSerie(int id) {
		ClientResponse response = null;
		try {
			String url = marvelSettings.mountSeriesUrl(id);
			String etagMapKey = String.format("findSerie_%d", id);

			Map<String, String> headers = new HashMap<>();

			if (etagMap.containsKey(etagMapKey)) {
				headers.put("If-None-Match", etagMap.get(etagMapKey));
			}

			response = sendRequest(url, headers);

			if (response.getStatus() == 304) {
				return marvelSerieMap.get(id);
			}

			String json = response.getEntity(String.class);

			List<MarvelSerie> marvelSeries = parseSerieJSON(etagMapKey, json);

			if (marvelSeries.isEmpty()) {
				return null;
			}

			return marvelSeries.get(0);
		} catch (Exception ex) {
			throw new MarvelServiceException("An error occured while finding a serie by id.", ex);
		} finally {
			try {
				response.close();
			} catch (Exception ex) {
				throw new MarvelServiceException("An error occured while finding a serie by id.", ex);
			}
		}
	}

}

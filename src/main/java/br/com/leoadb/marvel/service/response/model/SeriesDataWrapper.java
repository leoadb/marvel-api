package br.com.leoadb.marvel.service.response.model;

public class SeriesDataWrapper {
	private int code;
	private SeriesDataContainer data;
	private String etag;

	public int getCode() {
		return code;
	}

	public SeriesDataContainer getData() {
		return data;
	}

	public String getEtag() {
		return etag;
	}

}

package br.com.leoadb.marvel.service;

import java.util.List;

import br.com.leoadb.marvel.model.MarvelSerie;

public interface MarvelSerieService {

	public List<MarvelSerie> findSeries(int offset, int limit);
	
	public MarvelSerie findSerie(int id);
}

package br.com.leoadb.marvel.service.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.leoadb.marvel.model.MarvelCreator;
import br.com.leoadb.marvel.model.MarvelCreatorComic;
import br.com.leoadb.marvel.model.MarvelCreatorEvent;
import br.com.leoadb.marvel.model.MarvelCreatorSerie;
import br.com.leoadb.marvel.model.MarvelCreatorStory;
import br.com.leoadb.marvel.model.MarvelSerie;
import br.com.leoadb.marvel.model.MarvelSerieCreator;
import br.com.leoadb.marvel.service.response.model.ComicList;
import br.com.leoadb.marvel.service.response.model.ComicSummary;
import br.com.leoadb.marvel.service.response.model.Creator;
import br.com.leoadb.marvel.service.response.model.CreatorDataContainer;
import br.com.leoadb.marvel.service.response.model.CreatorDataWrapper;
import br.com.leoadb.marvel.service.response.model.CreatorSummary;
import br.com.leoadb.marvel.service.response.model.EventList;
import br.com.leoadb.marvel.service.response.model.EventSummary;
import br.com.leoadb.marvel.service.response.model.Series;
import br.com.leoadb.marvel.service.response.model.SeriesDataContainer;
import br.com.leoadb.marvel.service.response.model.SeriesDataWrapper;
import br.com.leoadb.marvel.service.response.model.SeriesList;
import br.com.leoadb.marvel.service.response.model.SeriesSummary;
import br.com.leoadb.marvel.service.response.model.StoryList;
import br.com.leoadb.marvel.service.response.model.StorySummary;

@Component
public class MarvelDataExtractor {

	public List<MarvelSerie> getSeries(SeriesDataWrapper seriesDataWrapper) {
		List<MarvelSerie> marvelSeries = new ArrayList<>();
		SeriesDataContainer seriesDataContainer = seriesDataWrapper.getData();
		Series[] series = seriesDataContainer.getResults();
		for (int i = 0; i < series.length; i++) {
			MarvelSerie marvelSerie = new MarvelSerie(series[i].getId(), series[i].getTitle(),
					series[i].getDescription(), series[i].getStartYear(), series[i].getEndYear());

			CreatorSummary[] creatorSummaries = series[i].getCreators().getItems();

			for (CreatorSummary creatorSummary : creatorSummaries) {
				marvelSerie.getCreators()
						.add(new MarvelSerieCreator(creatorSummary.getName(), creatorSummary.getRole()));
			}

			marvelSeries.add(marvelSerie);
		}
		return marvelSeries;
	}

	public List<MarvelCreator> getCreators(CreatorDataWrapper creatorDataWrapper) {
		List<MarvelCreator> marvelCreators = new ArrayList<>();
		CreatorDataContainer creatorDataContainer = creatorDataWrapper.getData();
		Creator[] creators = creatorDataContainer.getResults();
		for (int i = 0; i < creators.length; i++) {
			MarvelCreator marvelCreator = new MarvelCreator(creators[i].getId(), creators[i].getFullName());

			SeriesList seriesList = creators[i].getSeries();
			if (seriesList != null) {
				SeriesSummary[] seriesSummaries = seriesList.getItems();
				for (SeriesSummary seriesSummary : seriesSummaries) {
					marvelCreator.getSeries().add(new MarvelCreatorSerie(seriesSummary.getName()));
				}
			}

			EventList eventList = creators[i].getEvents();
			if (eventList != null) {
				EventSummary[] eventSummaries = eventList.getItems();
				for (EventSummary eventSummary : eventSummaries) {
					marvelCreator.getEvents().add(new MarvelCreatorEvent(eventSummary.getName()));
				}
			}

			ComicList comicList = creators[i].getComics();
			if (comicList != null) {
				ComicSummary[] comicSummaries = comicList.getItems();
				for (ComicSummary comicSummary : comicSummaries) {
					marvelCreator.getComics().add(new MarvelCreatorComic(comicSummary.getName()));
				}
			}

			StoryList storyList = creators[i].getStories();
			if (storyList != null) {
				StorySummary[] storySummaries = storyList.getItems();
				for (StorySummary storySummary : storySummaries) {
					marvelCreator.getStories().add(new MarvelCreatorStory(storySummary.getName()));
				}
			}

			marvelCreators.add(marvelCreator);
		}
		return marvelCreators;
	}
}

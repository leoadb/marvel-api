package br.com.leoadb.marvel.service.impl;

import java.util.Map;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

abstract class MarvelBaseService {
	protected ClientResponse sendRequest(String url, Map<String, String> headers) {
		Client client = Client.create();

		WebResource webResource = client.resource(url);

		Builder builder = webResource.accept("*/*");

		for (Map.Entry<String, String> entry : headers.entrySet()) {
			builder = builder.header(entry.getKey(), entry.getValue());
		}

		return builder.get(ClientResponse.class);
	}
}
